﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public static AudioManager Instance = null;
	public AudioClip[] deathSFX;
	public AudioClip[] buildingDestroyedSFX;
	public AudioClip[] stompSFX;

	public AudioClip victorySFX;

	public GameObject currentTrackInstance;
	public AudioClip currentTrack;
	public AudioClip gameplayTrack;
	public AudioClip menuTrack;

	void Awake(){
		if (Instance == null){
			Instance = GameObject.FindObjectOfType<AudioManager>();
		} else if (Instance != this){
			Destroy (gameObject);
		}
		DontDestroyOnLoad(gameObject);

		PlayerPrefs.SetInt("Sound", 1);
		PlayerPrefs.SetInt("Music", 1);
	}

	public static void PlayAudioClipStatic(AudioClip clip, float volume = 1f){
		if(PlayerPrefs.GetInt("Sound") == 1){
			GameObject tempGO = new GameObject("TempAudio"); // create the temp object
			tempGO.transform.position = Vector3.zero; // set its position
			AudioSource audio = tempGO.AddComponent<AudioSource>(); // add an audio source
			audio.clip = clip; // define the clip
			audio.Play(); // start the sound
			audio.volume = volume; // start the sound
			audio.spatialBlend = 0f;
			Destroy(tempGO, audio.clip.length);
		}
	}

	public void PlayAudioClipAtPoint(AudioClip clip, Vector3 position){
		AudioSource.PlayClipAtPoint(clip, position);
	}

	public void PlayAudioClip(AudioClip[] clips, float volume = 1f, bool randomPitch = false){
		PlayAudioClip(clips[Random.Range(0, clips.Length)], volume, randomPitch);
	}

	public void PlayAudioClip(AudioClip clip, float volume = 1f, bool randomPitch = false){
		if(PlayerPrefs.GetInt("Sound") == 1){
			GameObject tempGO = new GameObject("TempAudio"); // create the temp object
			tempGO.transform.parent = transform;
			tempGO.transform.position = Vector3.zero; // set its position
			AudioSource audio = tempGO.AddComponent<AudioSource>(); // add an audio source
			audio.clip = clip; // define the clip
			if(randomPitch){
				audio.pitch *= (Random.Range(1f, 1.12f));
			}
			audio.Play(); // start the sound
			audio.volume = volume; // start the sound
			audio.spatialBlend = 0f;
			Destroy(tempGO, audio.clip.length);
		}
	}

	public void PlayAudioClip3D(AudioClip c){
		if(PlayerPrefs.GetInt("Sound") == 1){
			AudioSource.PlayClipAtPoint(c, Camera.main.transform.position);
		}
	}

	public void PlayAudioClip3D(AudioClip c, float volume){
		if(PlayerPrefs.GetInt("Sound") == 1){
			AudioSource.PlayClipAtPoint(c, Camera.main.transform.position, volume);
		}
	}	


	public static void PlayMenuTrack(){
		if(PlayerPrefs.GetInt("Music") == 1){
			if(Instance == null){
				Instance = GameObject.FindObjectOfType<AudioManager>();
			}
			Instance.currentTrack = Instance.menuTrack;
			Instance.currentTrackInstance = Instance.PlayClipReference(Instance.currentTrack, 0.75f, true);
		}
	}

	public static void PlayGameplayTrack(){
		if(PlayerPrefs.GetInt("Music") == 1){
			if(Instance == null){
				Instance = GameObject.FindObjectOfType<AudioManager>();
			}

			if(Instance.currentTrack == null){
				Instance.currentTrack = Instance.gameplayTrack;
				Instance.currentTrackInstance = Instance.PlayClipReference(Instance.currentTrack, 0.75f, true);
			}
		}
	}

	public void StopCurrentTrack(){
		if(currentTrackInstance != null){
			currentTrackInstance.GetComponent<AudioSource>().Stop();
			Destroy(currentTrackInstance.gameObject);
			currentTrackInstance = null;
		}
	}

	public GameObject PlayClipReference(AudioClip clip, float volume = 1f, bool loop = false){
		GameObject tempGO = new GameObject("TempAudio"); // create the temp object
		tempGO.transform.parent = transform;
		tempGO.transform.position = Vector3.zero; // set its position
		AudioSource audio = tempGO.AddComponent<AudioSource>(); // add an audio source
		audio.loop = loop;
		audio.clip = clip; // define the clip
		audio.Play(); // start the sound
		audio.volume = volume; // start the sound
		//Destroy(tempGO, clip.length); // destroy object after clip duration
		return tempGO; // return reference to the temporary GameObject
	}		
}