﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScreen : MonoBehaviour {
	public Text killCountLabel;
	public Text collateralDamageLabel;
	public Text timerLabel;
	public Text titleLabel;
	public Text phrase1Label;
	public Text phrase2Label;

	public void Open(string title, string phrase1, string phrase2){
		gameObject.SetActive (true);
		titleLabel.text = title;
		phrase1Label.text = phrase1;
		phrase2Label.text = phrase2;
		killCountLabel.text = GameManager.KillCount.ToString ();
		collateralDamageLabel.text = GameManager.CollateralDamage.ToString () + "k";

		System.TimeSpan ts = new System.TimeSpan (0, 0, Mathf.RoundToInt(GameManager.TimeLeft));
		string s = string.Format ("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
		timerLabel.text = s;
	}
}
