﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialArea : MonoBehaviour {
	public string msg;

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag.Equals ("Player")) {
			UIManager.Instance.ShowTutorialMessage (msg);
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if(col.gameObject.tag.Equals("Player")){
			UIManager.Instance.HideTutorialMessage ();
		}
	}
}
