﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;
	public Villain villain;
	public Transform giant;
	public Transform[] legs;
	public static int KillCount = 0;
	public static int CollateralDamage = 0;
	public static float SecondsPlaying = 0f;
	public static float TimeLeft = 0f;
	public Coroutine startStoryCoroutine;

	void Awake(){
		Instance = GameObject.FindObjectOfType<GameManager> ();
		Camera cam = Camera.main;
		cam.GetComponent<CameraFollow> ().enabled = false;
		//cam.transform.position = new Vector3(GameObject.FindObjectOfType<Villain> ().transform.position.x, cam.transform.position.y, cam.transform.position.z);
	}

	void Start(){
		AudioManager.PlayGameplayTrack ();
	}

	public void StartStory(){
		startStoryCoroutine = StartCoroutine(UIManager.Instance.StartStory());
	}

	public void QuitGame(){
		Application.Quit ();
	}
		
	public void RestartLevel(){
		Application.LoadLevel (0);
	}

	public void StartGame(){
		KillCount = 0;
		CollateralDamage = 0;
		SecondsPlaying = 0;
		TimeLeft = 90f;
		UIManager.Instance.phase = UIManager.Phase.Gameplay;
		UIManager.Instance.UpdateKillCount ();
		UIManager.Instance.UpdateCollateralDamage ();
		UIManager.Instance.UpdateTimer ();
		Camera.main.GetComponent<CameraFollow> ().enabled = true;
	}

	public Vector3 GetGiantPosition(){
		return giant.transform.position;
	}

	public float GetDistanceFromOtherLeg(Transform t){
		for(int i = 0; i < legs.Length; i++){
			if (!t.name.Equals(legs[i].name)){
				return Mathf.Abs (t.position.x - legs[i].position.x);
			}
		} 
		return 10f;
	}

	public float GetXPositionFromOtherLeg(Transform t){
		for(int i = 0; i < legs.Length; i++){
			if (!t.name.Equals(legs[i].name)){
				return legs[i].transform.position.x;
			}
		} 
		return 10f;		
	}

	public static void IncreaseKillCount(int i){
		KillCount += i;
		UIManager.Instance.UpdateKillCount();
	}

	void Update(){
		if (UIManager.Instance.phase == UIManager.Phase.Gameplay) {
			SecondsPlaying += Time.deltaTime;
			TimeLeft -= Time.deltaTime;
			UIManager.Instance.UpdateTimer();

			if (TimeLeft <= 0) {
				GameOver ();
			}
		}
	}

	public static void IncreaseCollateralDamage(int i){
		CollateralDamage += i;
		UIManager.Instance.UpdateCollateralDamage();
	}

	public Transform GetClosestLeg(Vector3 personPosition){
		Transform closestLeg = null;
		float smallestDistance = Mathf.Infinity;
		foreach (Transform t in legs) {
			float distance = t.position.x + 1f - personPosition.x;
			if (Mathf.Abs(distance) < smallestDistance) {
				smallestDistance = Mathf.Abs(distance);
				closestLeg = t;
			}
		}
		return closestLeg;
	}

	public void FinishGame(){
		UIManager.Instance.phase = UIManager.Phase.GameOver;
		UIManager.Instance.victoryScreen.Open ("Congratulations", "You saved the city!", (KillCount > 0 || CollateralDamage > 0) ? "OF COURSE, THERE ARE SOME CASUALTIES..." : "LOOKS LIKE TOEZILLA DIDN'T KILL ANYONE!" );
	}

	public void GameOver(){
		foreach (Transform t in legs) {
			t.GetComponent<Leg> ().enabled = false;
		}
		UIManager.Instance.phase = UIManager.Phase.GameOver;
		UIManager.Instance.victoryScreen.Open ("Game Over", "Toezilla didn't make it on time!", (KillCount > 0 || CollateralDamage > 0) ? "" : "BUT STILL DESTROYED THE CITY");		
	}
}
