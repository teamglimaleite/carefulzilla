﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToe : MonoBehaviour {
	public Collider2D[] collidersToIgnore;
	private Collider2D col;

	void Awake(){
		col = GetComponent<Collider2D> ();
		foreach (Collider2D c in collidersToIgnore) {
			Physics2D.IgnoreCollision(c, col);
		}
	}
}
