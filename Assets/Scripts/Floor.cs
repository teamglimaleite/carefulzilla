﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Floor : MonoBehaviour {
	public GameObject smokePrefab;

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.relativeVelocity.magnitude > .5f && coll.collider.gameObject.tag.Equals("Player")) {
			AudioManager.Instance.PlayAudioClip (AudioManager.Instance.stompSFX, 1f, true);
			foreach (ContactPoint2D contact in coll.contacts) {
				Vector3 v3 = new Vector3 (contact.point.x, contact.point.y, 0);
				GameObject smoke = Instantiate (smokePrefab) as GameObject;
				smoke.transform.position = v3;
			}
			Camera.main.transform.DOShakePosition (0.1f, 0.025f);

		}
	}
}
