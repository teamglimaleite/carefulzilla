﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foot : MonoBehaviour {
	public Leg leg;

	void Start(){
		//leg = transform.parent.GetComponent<Leg> ();
	}

	void OnMouseDown(){
		leg.OnMouseDown ();
	}

	void OnMouseUp(){
		leg.OnMouseUp ();
	}

	void OnMouseDrag(){
		leg.OnMouseDrag ();
	}
}
