﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leg : MonoBehaviour {
	public State state;
	public Rigidbody2D legRB;
	public Rigidbody2D footRB;
	public Rigidbody2D toeRB;

	[Range(1f, 20f)]
	public float mouseSensitivity = 3f;
	float xLimit = 0f;

	public enum State{
		Released,
		BeingDragged
	}

	public void OnMouseDown(){
		legRB.bodyType = RigidbodyType2D.Kinematic;
		state = State.BeingDragged;
		xLimit = GameManager.Instance.GetXPositionFromOtherLeg(transform);
		Debug.Log(GameManager.Instance.GetXPositionFromOtherLeg(transform));
	}

	public void OnMouseUp(){
		legRB.bodyType = RigidbodyType2D.Dynamic;
		state = State.Released;	
	}

	public void OnMouseDrag(){
		float maxDistanceBetweenLegs = 6f;
		Vector3 targetPos = (Vector2)legRB.position + new Vector2 (Input.GetAxis ("Mouse X"), Input.GetAxis ("Mouse Y")) * Time.fixedDeltaTime * mouseSensitivity;
		targetPos.y = Mathf.Max (0.25f, targetPos.y);
		targetPos.x = Mathf.Clamp(targetPos.x, xLimit - maxDistanceBetweenLegs, xLimit + maxDistanceBetweenLegs);
		legRB.MovePosition(targetPos);
	}
}