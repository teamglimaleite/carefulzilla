﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour {
	public static UIManager Instance;
	public RectTransform[] balloons; 
	public GameObject gameplayUI;
	public Graphic[] mainMenuGraphics;
	int balloonIndex = 0;
	public Phase phase;

	[Header("UI Refs.")]
	public Text killCountLabel;
	public Text collateralDamageLabel;
	public Text timerLabel;
	public EndGameScreen victoryScreen;
	public GameObject tutorialBalloon;
	public GameObject skipButton;

	public enum Phase{
		MainMenu,
		Story,
		Gameplay,
		GameOver
	}

	void Awake(){
		Instance = GameObject.FindObjectOfType<UIManager> ();
		gameplayUI.SetActive (false);
	}

	public void UpdateKillCount(){
		killCountLabel.text = GameManager.KillCount.ToString ();
	}

	public void UpdateTimer(){
		System.TimeSpan ts = new System.TimeSpan (0, 0, Mathf.RoundToInt(GameManager.TimeLeft));
		string s = string.Format ("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
		timerLabel.text = s;
	}

	public void UpdateCollateralDamage(){
		collateralDamageLabel.text = GameManager.CollateralDamage.ToString () + "k";
	}

	public void TweenCameraToPosition(Vector3 v3){
		Camera cam = Camera.main;
		cam.transform.DOMove(new Vector3(v3.x, cam.transform.position.y, cam.transform.position.z), 2f);
	}

	public IEnumerator TweenCameraToPositionCoroutine(Vector3 v3){
		Camera cam = Camera.main;
		cam.transform.DOMove(new Vector3(v3.x, cam.transform.position.y, cam.transform.position.z), 2f);		
		yield return new WaitForSeconds(2f);
	}

	public IEnumerator StartStory(){
		Camera cam = Camera.main;


		foreach (Graphic graph in mainMenuGraphics) {
			graph.gameObject.SetActive (false);
		}
		skipButton.SetActive(true);

		yield return StartCoroutine(TweenCameraToPositionCoroutine(new Vector3(GameObject.FindObjectOfType<Villain> ().transform.position.x - 2f, 0, 0)));
		phase = Phase.Story;
		balloonIndex = -1;
		ShowNextBalloon ();
	}

	void Update(){
		switch (phase) {
			case(Phase.MainMenu):
				break;
			case(Phase.Story):
				
				if (Input.anyKeyDown) {
					ShowNextBalloon();
				}
				break;
			}
	}

	public void ShowNextBalloon(){
		if (balloonIndex + 1 < balloons.Length) {
			HideAllBalloons ();
			balloons [balloonIndex + 1].gameObject.SetActive (true);
			balloonIndex++;
		} else {
			HideAllBalloons ();
			StartCoroutine(StoryEnded ());
		}
	}

	void HideAllBalloons(){
		foreach (RectTransform r in balloons) {
			r.gameObject.SetActive (false);
		}
	}

	public void ShowTutorialMessage(string s){
		tutorialBalloon.gameObject.SetActive (true);
		tutorialBalloon.transform.GetChild(0).GetComponentInChildren<Text> ().text = s;
	}

	public void HideTutorialMessage(){
		Graphic[] graphics = tutorialBalloon.transform.GetChild (0).GetComponentsInChildren<Graphic> ();
		foreach (Graphic g in graphics) {
			g.CrossFadeAlpha (0f, 0.5f, true);
		}
	}

	public void SkipStory(){
		GameManager.Instance.StopCoroutine(GameManager.Instance.startStoryCoroutine);
		HideAllBalloons ();
		StartCoroutine(StoryEnded ());
	}

	public IEnumerator StoryEnded(){
		skipButton.SetActive(false);
		CameraFollow camFollow = Camera.main.GetComponent<CameraFollow>();
		Vector3 v3 = camFollow.CameraTargetPosition ();
		yield return StartCoroutine(TweenCameraToPositionCoroutine(v3));
		HideAllBalloons();
		camFollow.enabled = true;
		gameplayUI.SetActive (true);
		GameManager.Instance.StartGame ();
		GameManager.Instance.villain.GoToIdleAnimation();
		ShowTutorialMessage ("DRAG CAREFULZILLA'S LEGS TO MOVE.");
	}

	public void ShowKillCountNumber(int kc, Transform t){
		GameObject go = Instantiate (Resources.Load ("UI/KillCountNumber")) as GameObject;
		go.transform.position = t.position + new Vector3(0, t.GetComponent<Collider2D>().bounds.size.y, 0);
		go.transform.DOMoveY (go.transform.position.y + 0.2f, 1f);
		go.GetComponentInChildren<Text> ().text = kc.ToString ();
		foreach (Transform child in go.transform) {
			Graphic graphic = child.GetComponent<Graphic> ();
			if (graphic != null) {
				Color c = graphic.color;
				c.a = 0;
				graphic.color = c;
				graphic.CrossFadeAlpha (1f, 0.5f, true);
			}
		}
		StartCoroutine(DestroyAlpha(go, 1f));
	}

	IEnumerator DestroyAlpha(GameObject go, float delay){
		yield return new WaitForSeconds (delay);
		Graphic[] gs = go.transform.GetChild (0).GetComponentsInChildren<Graphic> ();
		foreach (Graphic graphic in gs) {
			graphic.CrossFadeAlpha (0f, 0.5f, true);
		}
		Destroy (go, 0.5f);
	}

	public void ShowCollateralCountNumber(int kc, Transform t){
		GameObject go = Instantiate (Resources.Load ("UI/CollateralCountNumber")) as GameObject;
		go.transform.position = t.position + new Vector3(0, t.GetComponent<Collider2D>().bounds.size.y + 0.3f, 0);
		go.transform.DOMoveY (go.transform.position.y + 0.2f, 1f);
		go.GetComponentInChildren<Text> ().text = kc.ToString () + "k";
		foreach (Transform child in t) {
			Graphic graphic = child.GetComponent<Graphic> ();
			if (graphic != null) {
				Color c = graphic.color;
				c.a = 0;
				graphic.color = c;
				graphic.CrossFadeAlpha (1f, 0.5f, true);
			}
		}
		StartCoroutine(DestroyAlpha(go, 2f));
	}
}
