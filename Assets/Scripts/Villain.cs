﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Villain : MonoBehaviour {
	public int hitPoints = 3;
	bool chasing = false;
	bool beingHit = false;
	Renderer renderer;

	void Start(){
		renderer = GetComponentInChildren<Renderer> ();
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.relativeVelocity.magnitude > 3f) {
			StartCoroutine (Hit ());

		}
	}

	IEnumerator Hit(){
		if (beingHit)
			yield break;

		beingHit = true;
		hitPoints--;

		GetComponent<Animator> ().SetTrigger ("Hit");
		if (hitPoints <= 0) {
			GetComponent<Joint2D> ().enabled = false;
			GetComponent<Rigidbody2D> ().gravityScale = 1f;
			GameManager.Instance.FinishGame ();
			yield break;
		}
		yield return new WaitForSeconds (1f);
		beingHit = false;
		GetComponent<Animator> ().SetTrigger ("RecoveredFromDamage");
	}

	public void GoToIdleAnimation(){
		GetComponent<Animator> ().SetBool ("Alive", true);
	}

	void Update(){
		if (UIManager.Instance.phase == UIManager.Phase.Gameplay) {
			if (chasing) {
				if (renderer.isVisible) {
					//transform.Translate (Vector3.right * 0.75f * Time.deltaTime);
				}
			} else {
				if (renderer.isVisible) {
					float distance = Mathf.Abs (transform.position.x - GameManager.Instance.GetClosestLeg (transform.position).position.x);
					if (distance < 3f) {
						chasing = true;
					}
				}
			}			
		} 
	}
}