﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Building : MonoBehaviour {
	Rigidbody2D rb;
	public List<Collider2D> pieces;
	bool hasDestroyed = false;
	bool isReady = false;
	Vector2 force;

	void Start(){
		rb = GetComponent<Rigidbody2D> ();
		pieces = new List<Collider2D>(GetComponentsInChildren<Collider2D> (true));
		pieces.RemoveAt (0);
		StartCoroutine (GetReady ());
	}

	IEnumerator GetReady(){
		while (rb.velocity.magnitude > 0.1f) {
			yield return null;
		}
		isReady = true;
	}

	public void DestroyBuilding(){
		AudioManager.Instance.PlayAudioClip (AudioManager.Instance.buildingDestroyedSFX, 1f, true);
		int k = Random.Range (20, 100);
		int g = Random.Range (20, 100);
		GameManager.IncreaseKillCount(k);
		GameManager.IncreaseCollateralDamage(g);
		UIManager.Instance.ShowKillCountNumber(k, transform);
		UIManager.Instance.ShowCollateralCountNumber(g, transform);

		hasDestroyed = true;
		GetComponent<Rigidbody2D> ().isKinematic = true;
		GetComponent<Collider2D> ().enabled = false;
		GetComponent<SpriteRenderer> ().enabled = false;
		foreach (Collider2D piece in pieces) {
			piece.attachedRigidbody.AddRelativeForce (force);
			piece.gameObject.layer = 10;
			piece.gameObject.SetActive (true);
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (isReady && (col.relativeVelocity.magnitude > 3f || col.relativeVelocity.y > 0.65f) && col.collider.gameObject.tag.Equals("Player")) {
			force = col.relativeVelocity;
			DestroyBuilding ();
		}

	}
		
}
