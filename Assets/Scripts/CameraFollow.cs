﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public Transform[] legs;
	public float smoothingThreshold;
	public float xOffset;
	float originalY;

	void Start(){
		originalY = transform.position.y;
	}

	public Vector3 CameraTargetPosition(){
		Vector3 avgPos = new Vector3 ();
		for (int i = 0; i < legs.Length; i++) {
			avgPos += legs [i].position;
		}
		avgPos = avgPos / legs.Length;
		return avgPos;
	}


	void Update(){
		Vector3 avgPos = CameraTargetPosition ();
		transform.position = Vector3.Lerp (transform.position, new Vector3 (avgPos.x + xOffset, originalY, transform.position.z), smoothingThreshold * Time.deltaTime);
	}
}
