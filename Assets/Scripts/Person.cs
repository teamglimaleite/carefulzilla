﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour {

	public Color[] colors;
	public Sprite[] sprites;

	private float speed = 0.4f;
	private float startingX;

	void OnCollisionEnter2D(Collision2D col){
		if (col.relativeVelocity.y > 0.2f) {
			Die ();
		}
	}

	void Start(){
		startingX = transform.position.x;
		SpriteRenderer sr = GetComponent<SpriteRenderer> ();
		sr.color = colors[Random.Range (0, colors.Length)];
		sr.sprite = sprites[Random.Range(0, sprites.Length)];
		speed *= Random.Range (0.8f, 1.2f);
		//StartCoroutine ("WalkCycle");
	}

	void Die(){
		//StopCoroutine ("WalkCycle");
		AudioManager.Instance.PlayAudioClip (AudioManager.Instance.deathSFX, 1f, true);
		UIManager.Instance.ShowKillCountNumber(1, transform);
		GameManager.IncreaseKillCount(1);
		transform.localEulerAngles = new Vector3 (0, 0, 90f);
		GetComponent<Collider2D> ().enabled = false;
		this.enabled = false;
	}

	void Update(){
		
		Transform runAwayFrom = GameManager.Instance.GetClosestLeg(transform.position);
		float dir = Mathf.Sign (transform.position.x - runAwayFrom.position.x);
		float mySpeed = Mathf.Abs (speed) * dir;

		if(Mathf.Abs(runAwayFrom.transform.position.x - transform.position.x) < 5f)
			transform.Translate (Vector3.right * mySpeed * Time.deltaTime);
	}
}
