﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour {
	Renderer rend;
	float speed = 0.1f;
	// Use this for initialization
	void Start () {
		speed *= Random.Range (0.9f, 1.2f);
		rend = GetComponent<Renderer> ();	
	}
	
	// Update is called once per frame
	void Update () {
		if (rend.isVisible) {
			transform.Translate (Vector3.right * speed * Time.deltaTime);
		}
	}
}
