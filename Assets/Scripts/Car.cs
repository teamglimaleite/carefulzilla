﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {
	public Sprite[] sprites;

	void Start(){
		GetComponent<SpriteRenderer> ().sprite = sprites [Random.Range (0, sprites.Length)];
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.relativeVelocity.y > 0.2f) {
			Die ();
		}
	}

	void Die(){
		AudioManager.Instance.PlayAudioClip (AudioManager.Instance.deathSFX, 1f, true);
		int r = Random.Range (2, 10);
		GameManager.IncreaseCollateralDamage(r);
		UIManager.Instance.ShowCollateralCountNumber(r, transform);
		transform.localScale = new Vector3 (1f, 0.15f, 1f);
		GetComponent<Collider2D> ().enabled = false;
		this.enabled = false;
	}
}
